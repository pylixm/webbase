#webbase



## 说明 

### 项目功能

一般hr,erp,财务,进销存... 等一系列业务系统都需要有的共通的功能,例如:
* 员工管理
* 组织机构管理
* 流程申请
* 申请汇总
* excel导出

等. 
本项目包括以上功能,可在此基础上继续开发.

###使用技术
* springmvc
* mybatis
* shiro
* bootstrap
* bui
* dwr
* jquery

### 部署环境前提
* eclipse
* jdk7
* git
* tomcat
* mysql

### 项目配置

下载地址: https://git.oschina.net/wangt/webbase.git

导入到eclipse中的具体配置同一般web项目.有问题可联系 qq:313949522.

项目访问目录为 main.jsp.

###相关文章

[Spring SpringMvc 3.0 + MyBatis 整合 ](http://blog.csdn.net/mamba10/article/details/17592981)


[Spring SpringMvc 3.0 + MyBatis 整合--补充关于.properties文件的读取 ](http://blog.csdn.net/mamba10/article/details/45055917)

[dwr 3.0 配置 + 不同参数，不同返回值调用方法 示例 ](http://blog.csdn.net/mamba10/article/details/41847619)


## 演示地址
[http://webbase.aliapp.com/](http://webbase.aliapp.com/)

* admin/111111
* 000004/111111

### 联系我
* email: softti@163.com

###项目截图
![image](http://img.blog.csdn.net/20150416171149366?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQvbWFtYmExMA==/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/Center)

![image](http://img.blog.csdn.net/20150416171348771?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQvbWFtYmExMA==/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/Center)

![image](http://img.blog.csdn.net/20150416171401314?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQvbWFtYmExMA==/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/Center)

![image](http://img.blog.csdn.net/20150416171413965?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQvbWFtYmExMA==/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/Center)

![image](http://img.blog.csdn.net/20150416171424495?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQvbWFtYmExMA==/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/Center)

![image](http://img.blog.csdn.net/20150416171434510?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQvbWFtYmExMA==/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/Center)

![image](http://img.blog.csdn.net/20150416171443106?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQvbWFtYmExMA==/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/Center)

![image](http://img.blog.csdn.net/20150416171312436?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQvbWFtYmExMA==/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/Center)

![image](http://img.blog.csdn.net/20150416171501873?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQvbWFtYmExMA==/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/Center)

![image](http://img.blog.csdn.net/20150416171513978?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQvbWFtYmExMA==/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/Center)

![image](http://img.blog.csdn.net/20150416171523775?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQvbWFtYmExMA==/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/Center)




## 捐赠
如果您喜欢这个项目,可以捐赠支持一下
* 支付宝帐号: softti@163.com 
* 姓名: 王庭

### 捐赠记录
2015-04-15 康晓林
